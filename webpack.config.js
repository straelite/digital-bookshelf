const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const copyWebpackPlugin = require("copy-webpack-plugin");
const DotenvPlugin = require("dotenv-webpack")

module.exports = {
  entry: { app: "./src/app.ts", socket: "./src/socket.ts" },
  output: {
    path: path.join(__dirname, "dist"),
    publicPath: "/",
    filename: "[name].js",
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: "ts-loader",
        exclude: /node_modules/,
      },
      {
        test: /\.ejs$/,
        loader: 'ejs-loader',
        options: {
          variable: 'data',
          interpolate : '\\{\\{(.+?)\\}\\}',
          evaluate : '\\[\\[(.+?)\\]\\]'
        }
      }
    ],
  },
  resolve: {
    extensions: [".ts", ".js", ".json"],
    modules: [path.resolve(__dirname, "src"), "node_modules"],
  },
  target: "node",
  node: {
    __dirname: false,
    __filename: false,
  },
  plugins: [
    new HtmlWebpackPlugin({
        template: './src/resources/index.ejs',
        title: 'Album Art Gallery',
        output: path.resolve(__dirname, './dist/public')
    }),
    new copyWebpackPlugin({
      patterns: [{ from: "src/*json" , to: '[name].[ext]'}],
    }),
    new DotenvPlugin({
      path: __dirname + '/src/.env'
    }),
  ]
};
