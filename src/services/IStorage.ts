interface IStorage {
    saveFile(filename :string, extension: string): Promise<void>
    getRandomFile(): Promise<string>
}