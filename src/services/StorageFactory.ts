import LocalFileStorage from "./LocalFileStorage";

class StorageFactory {
    public getStorage(storageName: string): IStorage
    {
        switch (storageName) 
        {
            default:
            return new LocalFileStorage
        }
    }
} 
export default StorageFactory;