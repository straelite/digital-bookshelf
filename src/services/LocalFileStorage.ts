import * as fs from 'fs';
import * as path from 'path';
import config from '../config'

const axios = require('axios');
class LocalFileStorage implements IStorage {
  private imgSubdir = config.env === 'development' ? '../public/images' : 'public/images';
  private imgDirPath = path.resolve(__dirname, this.imgSubdir);

  constructor() {
    if (!fs.existsSync(this.imgDirPath)) {
      fs.mkdirSync(this.imgDirPath, {recursive: true});
    }
  }

  public async getRandomFile() {
    const publicPath = `${config.protocol}${config.host}:${config.serverPort}` 
    const imgUrlSlug = 'images';
    const files = await fs.promises.readdir(this.imgDirPath);
    const index = Math.floor(Math.random() * files.length)
    return `${publicPath}/${imgUrlSlug}/${files[index]}`;
  }
  /**
   * 
   * @param remoteFileName 
   * @param extension 'including prepended .'
   */
  async saveFile(remoteFileName: string, extension: string) {
    const filename = path.basename(remoteFileName) + extension;
    const files = await fs.promises.readdir(this.imgDirPath);

    if (files.find(element => element === filename)) {
      return;
    }

    if (files.length >= config.maxFilesToStore) {
      this.deleteRandomFile(files);
    }

    const fileContents = await this.readFile(remoteFileName);
    this.saveLocally(remoteFileName, filename, fileContents);

    return

  }

  private async deleteRandomFile(files: Array<string>) {
    const index = Math.floor(Math.random() * 10 / files.length);
    fs.unlink(path.resolve(`${this.imgDirPath}/${files[index]}`), (err) => {
      if (err) console.error(err);
    });
  }

  private async readFile(remoteFileName :string): Promise<string> {
    const fileContents = await axios.get(remoteFileName, { responseType: 'arraybuffer' });
    let buffer = Buffer.from(fileContents.data, 'binary').toString('base64');
    buffer = buffer.replace(/^data:image\/\w+;base64,/, '');
    return buffer
  }

  private async saveLocally(remoteFileName :string, filename :string, content :string): Promise<void> {
    return await fs.readFile(remoteFileName, (err, file) => {
      fs.writeFile(`${this.imgDirPath}/${filename}`, content, 'base64', (err) => {
        if (err) console.error(err);
      });
    });
  }
}
export default LocalFileStorage;