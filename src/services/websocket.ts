const webSocket = require('ws');
import config from '../config'

const webSocketServer = new webSocket.Server({
    port: config.socketPort,
    serverMaxWindowBits: 10, // Defaults to negotiated value.
    // Below options specified as default values.
    concurrencyLimit: 10, // Limits zlib concurrency for perf.
    threshold: 2048, // Size (in bytes) below which messages
    'Access-Control-Allow-Origin': '*'
});

export default webSocketServer;
