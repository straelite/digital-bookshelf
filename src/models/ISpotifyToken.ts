interface ISpotifyToken {
    accessToken:string;
    refreshToken:string;
}

export default ISpotifyToken;