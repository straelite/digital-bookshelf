interface IProvider {
    getImage(): Promise<string>
}

export default IProvider;