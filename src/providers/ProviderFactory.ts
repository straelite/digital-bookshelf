import IProvider from "./IProvider";
import SpotifyProvider from "./SpotifyProvider";
import FallbackProvider from "./FallbackProvider"

class ProviderFactory {
    public getProvider(providerName: string): IProvider
    {
        switch (providerName) 
        {
            case "spotify":
            return new SpotifyProvider

            default:
            return new FallbackProvider
        }
    }
} 
export default ProviderFactory;