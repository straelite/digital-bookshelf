import IProvider from "./IProvider";
import StorageFactory from "../services/StorageFactory";
import config from "../config";

class FallbackProvider implements IProvider {
    async getImage(): Promise<string> {
        const factory = new StorageFactory;
        const storage = factory.getStorage(config.storageProvider);
        const file =  await storage.getRandomFile();
        return file;
    }
}

export default FallbackProvider;