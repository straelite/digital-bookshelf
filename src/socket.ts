const cron = require('node-cron');
const fs  = require('fs');
const path = require('path');

import IActiveProvider from "./models/IActiveProvider";
import ProviderFactory from "./providers/ProviderFactory";
import webSocketServer from "./services/websocket";
import config from "./config";

let pollInterval: NodeJS.Timer;

const broadCast = async () => {
  //This file needs to be read & writeable for saving settings changes so cannot be converted to a packaged import
  const json:string = fs.readFileSync(path.resolve(__dirname, './activeProvider.json'), 'utf8');
  const providerObject:IActiveProvider = JSON.parse(json);
  const activeProviderName = providerObject.activeProvider;
  const providerFactory = new ProviderFactory();
  const provider = providerFactory.getProvider(activeProviderName);
  const image = await provider.getImage();
  webSocketServer.clients.forEach((ws) => {
    ws.send(image);
  });
}

const setTimer = () => {
  pollInterval = setInterval(() => {
    broadCast();
  }, config.timerInterval)
}

//If the loop has been killed for any reason, clear the interval and kick it off again at the top of the minute 
const task = cron.schedule('* 6-23 * * *', () => {
    //Overlapping intervals are the tool of the devil
    clearInterval(pollInterval);
    setTimer();
});
task.start();
